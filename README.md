# IRL Docker Image

Provides tools and services for IRL streaming with OBS Studio as Docker Container.

## Prerequisites

You will need:

* [OBS Studio](https://obsproject.com/download)
* [OBS Websocket](https://obsproject.com/forum/resources/obs-websocket-remote-control-of-obs-studio-made-easy.466/)
* [VLC](https://www.videolan.org/vlc/index.html)
* [git](https://git-scm.com/downloads)
* [Docker](https://docs.docker.com/docker-for-windows/install/)
* [Live U Solo](https://gosolo.tv)

## RTMP Endpoint

* Port `1935`

## Webserver

* Port `80`

## docker-compose.yml

It is strongly advised to make use of `docker-compose` to build the image and run the container. Please create a `docker-compose.override.yml` to adjust the required settings to your needs. Use your full RTMP target URL as `STREAMKEY`. It's recommended to include a secret in your URL, e.g. `rtmp://yourserver.invalid:1935/live?key=<YOURSECRET>`

You can find an example in: `./examples/docker-compose/docker-compose.override.example.yml`

### Container Handling

#### Clone the repo

Use `powershell`, `cmd` or an WSL2 instance and go to the desired location on your local machine (e.g. `c:\users\IRL\`) using `cd`. Then execute the following command:

``` lang="bash"
git clone https://gitlab.com/gpvkt/twitch-irl-docker.git
```

This process must be done only once. The repo content will be cloned into the subdirectory called `docker`.

### Start container (first time run)

Use the `powershell` or `cmd` to go to your repo directory on your local machine (e.g. `c:\users\IRL\docker\`) using `cd`. Remember to create your `docker-compose.override.yml`. Then simply run the following command to start the Container, if needed docker-compose will start the build process.

``` lang="bash"
docker-compose up
```

If the container starts successfully you should see something like (followed by more or less verbose status messages depending on your log level):

``` lang="bash"
rtmp_1  | fcgiwrap                         STARTING
rtmp_1  | ircbot                           STARTING
rtmp_1  | liveucollector                   STARTING
rtmp_1  | nginx                            STARTING
rtmp_1  | obsctl                           STARTING
rtmp_1  | php                              STARTING
rtmp_1  | rtmpcollector                    STARTING
```

### Update image

To update your Docker image run the following commands inside your repo directory (e.g. `c:\users\IRL\docker\`):

``` lang="bash"
git pull
docker-compose build
docker-compose up -d
```

### Troubleshooting

If something works wrong you can examine the status of your container by running `docker-compose logs`. To get even more informations you can switch inside your running container by executing `docker-compose exec rtmp /bin/zsh`. Once you're inside the container you can examine the logfiles of all processes using the following commands:

``` lang="bash"
cat /var/log/supervisor/nginx.log
cat /var/log/supervisor/irl.log
```

To see the status of the processes or to restart them you can use the following commands:

``` lang="bash"
supervisorctl status

supervisorctl restart nginx
supervisorctl restart php
supervisorctl restart fcgiwrap
supervisorctl restart obsctl
supervisorctl restart rtmpcollector
supervisorctl restart liveucollector
supervisorctl restart ircbot
```

If your docker image doesn't not reflect the expected changes after pulling an repo update run the following commands inside your repo directory:

``` lang="bash"
git pull origin master
docker-compose kill
docker-compose rm --force
docker-compose build
docker-compose up -d
```

## OBS Studio

After installation OBS Studio you need to add the [obs-websocket plugin](https://github.com/obsproject/obs-websocket). We recommend using the 64 bit versions. You don't need to expose the websocket port to the internet, but choose a strong password anyway.

IMPORTANT: There are two version of `obs-websocket`, you will need the `4.x.x-compat` version. We will update to 5.x.x in a future version of this project.

Also you need to add the required scenes in OBS, you will find an importable example in `./examples/obs/IRL.json`. Please note that the example uses an VLC source for RTMP, so you need to have VLC installed. If you use OBS 64 bit you will need to use VLC 64 bit as well.

IMPORTANT: The Names of Scene-, Filter-, Audiosources etc. will be used as IDs in the OBS Web Remote App, therefore they MUST NOT contain any spaces and special Characters (like `"'|/\^<>?*#'&%$()[]{}§!,.`).

Please note that LiveU uses `/live/live` as default RTMP endpoint.

## OBS Web Remote

An webbased OBS Remote Interface is included. You can reach it via `http://localhost/remote.py`. The Interface is password protected. The username is `admin`, the password is identical to your `OBS_PASSWORD` you set in `docker-compose.override.yml`.

## Router/Firewall

You need to expose the RTMP Port (e.g. `1935`) and the webserver port (e.g. `80`) to the internet. Please configure your router and/or firewall accordingly. We also recommend to use an fixed IP Address for your Server, if you don't have one use an DynDNS service to get an domain name that always matches your current IP.

## TTS

You can integrate the [Simple TTS Bot](https://gitlab.com/gpkvt/twitchtts) into OBS Web Remote. Simply follow the installation process included in the bots README.md but use `localhost` as `http_bind` and `3000` as `http_port`.

Then add the following config to your `docker-compose.override.yml`:

``` lang="yaml"
# Integrate external TTS-Bot (see https://gitlab.com/gpvkt/twitchtts/)
- TTS=True
```
