#include "WiFi.h"
#include "HTTPClient.h"
#include "ArduinoJson.h"

#define ONBOARD_LED 2

const char* ssid = "<YOURWLANSSID";
const char* password = "<YOURWLANPASSWORD>";

String source = "http://m1login.net/api/model.json";
String target = "https://<YOURIRLSERVER>/set_ext.py";

unsigned long lastTime = 0;
unsigned long timerDelay = 5000;

void setup() {
  pinMode(ONBOARD_LED, OUTPUT);
  digitalWrite(ONBOARD_LED, LOW);
  Serial.begin(115200);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print("Connecting to WiFi");
    Serial.println(ssid);
  }
  
  Serial.print("Connected to the WiFi network: ");
  Serial.println(WiFi.localIP());
  digitalWrite(ONBOARD_LED, HIGH);
}

void loop() {
  if ((millis() - lastTime) > timerDelay) {
    if(WiFi.status() == WL_CONNECTED) {
      Serial.print("Fetching data from ");
      Serial.println(source.c_str());

      HTTPClient httpget;
      httpget.setFollowRedirects(HTTPC_STRICT_FOLLOW_REDIRECTS);
      httpget.begin(source.c_str());
      int httpResponseCode = httpget.GET();

      if (httpResponseCode>0) {
        Serial.print("GET Response code: ");
        Serial.println(httpResponseCode);

        String json = httpget.getString();
        //Serial.println(json);

        DynamicJsonDocument doc(32768);
        DeserializationError error = deserializeJson(doc, json);

        if (error) {
          Serial.println("Could not parse JSON data.");
          Serial.println(error.f_str());
          return;
        }

        String signalStrength = doc["wwan"]["signalStrength"]["bars"];
        String charging = doc["power"]["charging"];
        String battChargeLevel = doc["power"]["battChargeLevel"];
        String batteryVoltage = doc["power"]["batteryVoltage"];
        String batteryTemperature = doc["power"]["batteryTemperature"];
        String batteryTempState = doc["power"]["batteryTempState"];
        String devTemperature = doc["general"]["devTemperature"];
        String deviceTempCritical = doc["power"]["deviceTempCritical"];
        String connectionText = doc["wwan"]["connectionText"];
        String currentPSserviceType = doc["wwan"]["currentPSserviceType"];
        String registerNetworkDisplay = doc["wwan"]["registerNetworkDisplay"];

        String payload = "{ \"Connection\": \""+connectionText+"\", \"Service\": \""+currentPSserviceType+"\", \"Temp\": \""+devTemperature+"°C\", \"Accu\": \""+batteryTemperature+"°C\", \"Signal\": \""+signalStrength+"/5\", \"Charge\": \""+battChargeLevel+"%\", \"Voltage\": \""+batteryVoltage+"mV\", \"TempCrit\": \""+deviceTempCritical+"\", \"AccuCrit\": \""+batteryTempState+"\", \"Charging\": \""+charging+"\", \"Network\": \""+registerNetworkDisplay+"\" }";
        Serial.println(payload);

        Serial.print("Posting data to ");
        Serial.println(target.c_str());

        HTTPClient httppost;
        httppost.setFollowRedirects(HTTPC_STRICT_FOLLOW_REDIRECTS);
        httppost.begin(target.c_str());
        httppost.addHeader("Content-Type", "application/json");
        int postResponseCode = httppost.POST(payload);

        if (postResponseCode>0) {
          digitalWrite(ONBOARD_LED, HIGH);
          Serial.print("POST Response code: ");
          Serial.println(postResponseCode);

          String postload = httppost.getString();
          Serial.println(postload);
        } else {
          digitalWrite(ONBOARD_LED, LOW);
          Serial.print("POST Error code: ");
          Serial.println(postResponseCode);
        }
        httppost.end();
      } else {
        digitalWrite(ONBOARD_LED, LOW);
        Serial.print("HTTP Error code: ");
        Serial.println(httpResponseCode);
      }
      httpget.end();
    } else {
      Serial.println("WiFi disconnected");

      digitalWrite(ONBOARD_LED, LOW);
      WiFi.reconnect();
      delay(timerDelay);
      digitalWrite(ONBOARD_LED, HIGH);
    }
    lastTime = millis();
  }
}
