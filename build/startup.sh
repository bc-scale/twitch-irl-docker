#!/bin/bash

cp /var/www/html/auth.php /tmp/auth.php
/usr/bin/envsubst '${STREAMKEY}' < /tmp/auth.php > /var/www/html/auth.php
#cp /var/www/html/obs_settings.py /tmp/obs_settings.py
#/usr/bin/envsubst < /tmp/obs_settings.py > /var/www/html/obs_settings.py
cp /etc/nginx/nginx.conf /tmp/nginx.conf
/usr/bin/envsubst '${LETSENCRYPT_DOMAIN}' < /tmp/nginx.conf > /etc/nginx/nginx.conf
cp /etc/dehydrated/domains.txt /tmp/domains.txt
/usr/bin/envsubst '${LETSENCRYPT_DOMAIN}' < /tmp/domains.txt > /etc/dehydrated/domains.txt

echo ${OBS_PASSWORD} | /usr/bin/htpasswd -c -i /var/www/.htpasswd admin
echo "Password for Admin-Interface: ${OBS_PASSWORD}"

/etc/init.d/memcached start
chmod 777 /var/run/
rm /var/run/fcgiwrap.socket > /dev/null 2>&1

if [ ${LIVEU} != "True" ]; then
    echo "Removing LiveU service"
    rm /etc/supervisor/conf.d/liveu_collector.conf
fi

mkdir -p /var/www/html/.well-known/acme-challenge
mkdir -p /var/lib/dehydrated/certs/${LETSENCRYPT_DOMAIN}/
mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.full
mv /etc/nginx/nginx_tmp.conf /etc/nginx/nginx.conf
/usr/sbin/nginx
/usr/bin/dehydrated --register --accept-terms
/usr/bin/dehydrated -c
killall nginx
mv /etc/nginx/nginx.conf /etc/nginx/nginx_tmp.conf
mv /etc/nginx/nginx.conf.full /etc/nginx/nginx.conf

if [ -z ${LETSENCRYPT_DOMAIN} ]; then
  mv /etc/nginx/snakeoil.pem /var/lib/dehydrated/certs/${LETSENCRYPT_DOMAIN}/fullchain.pem
  mv /etc/nginx/snakeoil.key /var/lib/dehydrated/certs/${LETSENCRYPT_DOMAIN}/privkey.pem
fi

touch /opt/scripts/vol/__init__.py
/usr/bin/supervisord -c /etc/supervisor/supervisord.conf
/usr/bin/supervisorctl status
tail -f /var/log/supervisor/irl.log
