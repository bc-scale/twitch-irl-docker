#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Important: 

# Prefix any "text" with non ASCII chars with "u", e.g.:
# "äüöß" becomes u"äüöß"

# brb, live, irlstatus, irlclear, bitrate are reserved command keywords

# roles must contain any valid roles for the command (or "any")
# common roles are "broadcaster", "moderator", "any"

commands={ 
    "ping": { "roles": ["broadcaster", "moderator"], "text": [u"pong"] },
    "wo": { "roles": ["any"], "text": [u"EARTH!!! (most probably)", u"ZZ 9 Plural Z Alpha"] },
}
