#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    twitch-irl-docker
    Copyright (C) 2022  gpkvt

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import memcache
import json
import sys

from pprint import pprint

print("HTTP/1.0 200 OK")
print("Content-type:text/html;charset=utf-8\r\n")

json_error = False
mc = memcache.Client(['localhost:11211'], debug=0)

try:
    r = json.load(sys.stdin)
    json = json.dumps(r)
except:
    json_error = True

if not json_error:
    try:
        mc.set("ESP32", json)
        data = mc.get("ESP32")
    except:
        print('Could not set data')
else:
    print('Invalid request')

pprint(data)
