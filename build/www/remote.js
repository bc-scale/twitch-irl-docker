function jq(id) {
    return "#" + id.replace(/(\|)/g, "\\$1");
}

function clickButton({cmd=false, action=false, dayflat=false}) {
    console.log(dayflat);
    if (dayflat && cmd == "stream") {
        alert('Dayflat active?');
    }

    if (cmd && action) {
        $("body").addClass("loading");
        $.ajax({
            type: "post",
            url: "./set_obs.py",
            data: { "cmd": cmd, "action": action},
            cache: false,
            success: function() {
                console.log(cmd + " command with parameter "+ action +" was send successfully.");
            },
            error: function() {
                $('#ajaxerror').addClass("visible");
                $('#lowbitrate').removeClass("visible");
                $('#obserror').removeClass("visible");
            console.log('Could not set OBS status')
            }
        });
        setTimeout(function(){
            $("body").removeClass("loading");
        },5000);
    }
}

$(document).ready(function() {
    setInterval(function(){
        $.ajax({
            datatype: "json",
            url: "/data.py",
            cache: false,
            timeout: 2500,
            success: function(result) {
                $('#ajaxerror').removeClass("visible");

                var time = result.totalstreamtime / 60;
                $("#info").html("Dropped Frames:&nbsp;"+parseFloat(result.numdroppedframes).toFixed(0)+" - Live:&nbsp;"+parseFloat(time).toFixed(0)+"&nbsp;Minutes - CPU:&nbsp;"+parseFloat(result.cpuusage).toFixed(2)+"% - OBS&nbsp;In:&nbsp;"+parseFloat(result.bitrate).toFixed(0)+"kb/s - OBS&nbsp;Out:&nbsp;"+parseFloat(result.kbitspersec).toFixed(0)+"kb/s - Connections:&nbsp;"+parseFloat(result.connections).toFixed(0));
                if (result.liveu == "True") {
                    $("#liveu").html("Connected:&nbsp;"+result.batteryconnected+" - Charging:&nbsp;"+result.batterycharging+" - Discharging:&nbsp;"+result.batterydischarging+" - Time to empty:&nbsp;"+parseFloat(result.batterytime).toFixed(0)+"&nbsp;Minutes ("+parseFloat(result.batterypercentage).toFixed(2)+"%)");
                }
                
                if (result.lowbitrate == "True" ) {
                    $('#lowbitrate').addClass("visible");
                } else {
                    $('#lowbitrate').removeClass("visible");
                }

                $('#sceneform :button').removeClass("active");
                $('#' + result.currentscene).addClass("active");

                if (result.obsstatus == "False") {
                    $('#lowbitrate').removeClass("visible");
                    $('#ajaxerror').removeClass("visible");
                    $('#obserror').addClass("visible");
                    throw new Error('OBS CONNECTION ERROR');
                } else {
                    $('#obserror').removeClass("visible");
                }

                if (result.streamingstatus == "True") {
                    $('#startbutton').addClass("active");
                    $('#startbutton').text("Stop Stream");
                    $('#startbutton').attr("onclick", "clickButton({cmd: 'stream', action: 'stop'})");
                } else {
                    $('#startbutton').removeClass("active");
                    $('#startbutton').text("Start Stream");
                    if (result.dayflat) {
                        $('#startbutton').attr("onclick", "clickButton({cmd: 'stream', action: 'start', dayflat: 'True'})");
                    } else {
                        $('#startbutton').attr("onclick", "clickButton({cmd: 'stream', action: 'start'})");
                    }
                }

                if (result.activefilters) {
                    activefilters = result.activefilters.split(',');
                    for (i = 0; i < activefilters.length; i++) {
                        var btn = $(jq(activefilters[i]));
                        btn.addClass('active');
                        btn.text(activefilters[i] + ' (Active)');
                        btn.attr("onclick", "clickButton({cmd: 'source', action: 'filter|" +activefilters[i]+ "|0'})");
                    }
                }
                if (result.mutedfilters) {
                    mutedfilters = result.mutedfilters.split(',');
                    for (i = 0; i < mutedfilters.length; i++) {
                        var btn = $(jq(mutedfilters[i]));
                        btn.removeClass('active');
                        btn.text(mutedfilters[i] + ' (Disabled)');
                        btn.attr("onclick", "clickButton({cmd: 'source', action: 'filter|" +mutedfilters[i]+ "|1'})");
                    }
                }

                if (result.activeaudio) {
                    activeaudio = result.activeaudio.split(',')                    
                    for (i = 0; i < activeaudio.length; i++) {
                        var btn = $(jq(activeaudio[i]));
                        btn.addClass('active');
                        btn.text(activeaudio[i] + ' (Active)');
                    }
                }
                if (result.mutedaudio) {
                    mutedaudio = result.mutedaudio.split(',')
                    for (i = 0; i < mutedaudio.length; i++) {
                        var btn = $(jq(mutedaudio[i]));
                        btn.removeClass('active');
                        btn.text(mutedaudio[i] + ' (Muted)');
                    }
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                $('#ajaxerror').addClass("visible");
                $('#lowbitrate').removeClass("visible");
                $('#obserror').removeClass("visible");

                console.log("Error");
                console.log(xhr.status);
                console.log(thrownError);
            }
        });
    }, 1000);
});
